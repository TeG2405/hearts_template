module.exports = function(bh) {
    bh.match('pagination__link', function(ctx, json) {
        ctx
            .tag('a')
            .cls('page-link')
            .attr('href', '#');
    });
};
