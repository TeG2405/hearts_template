module.exports = function(bh) {
    bh.match('pagination', function(ctx, json) {
        ctx.tag('ul').content([
            new Array(json.length || 10).fill(null).map((item, index)=> [
                {elem: 'item', mix: {block: json.active == (index + 1) ? 'active': ''}, content: [
                    {elem: 'link', content: ++index},
                ]},
            ]),
        ]);
    });
};
