module.exports = function(bh) {
    bh.match('dropdown-menu', function(ctx, json) {
        ctx.content(
            ctx.content().map((item)=>{
                return ctx.isSimple(item) ? {elem: 'item', content: item} : item;
            }),
            true
        );
    });
};
