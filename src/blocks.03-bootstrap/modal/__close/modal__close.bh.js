module.exports = function(bh) {
    bh.match('modal__close', function(ctx, json) {
        ctx
            .tag('button')
            .attrs({
                'data-dismiss': 'modal',
            })
            .mix({block: 'close'})
            .content('&times;');
    });
};
