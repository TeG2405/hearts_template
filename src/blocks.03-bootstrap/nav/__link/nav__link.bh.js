module.exports = function(bh) {
    bh.match('nav__link', function(ctx, json) {
        ctx.tag('a').attr('href', '#').mix({block: 'nav-link'});
    });
};
