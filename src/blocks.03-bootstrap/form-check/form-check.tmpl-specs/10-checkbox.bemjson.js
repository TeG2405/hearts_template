module.exports = [
    {block: 'form-check', checked: true, content: 'Чекбокс выбранный'},
    {block: 'form-check', content: 'Чекбокс не выбранный'},
    {block: 'form-check', disabled: true, content: 'Чекбокс выбранный заблокированый'},
    {block: 'form-check', checked: true, disabled: true, content: 'Чекбокс выбранный заблокированый, выбраный'}
]
