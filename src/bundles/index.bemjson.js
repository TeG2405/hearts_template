module.exports = {
  block: 'page',
  title: 'Главная странциа',
  content: [
    require('./common/header.bemjson.js'),
    {block: 'main', content: [
      {block: 'container', content: [
        {block: 'mb-3', content: [
          require('./common/swiper-row-banners.bemjson'),
        ]},
        {block: 'mb-3', content: [
          {block: 'h', size: 1, cls: 'text-center', content: [
            {block: 'a', content: 'Товары со скидками'},
          ]},
          require('./common/swiper-row-multi.bemjson'),
          require('./common/swiper-row-multi.bemjson'),
        ]},
        {block: 'mb-3', content: [
          require('./common/map.bemjson'),
        ]},
        {block: 'mb-3', content: [
          {block: 'row', content: [
            {block: 'col-12', cls: 'col-lg-6 order-lg-1', content: [
              {block: 'h', size: '2', content: [
                {block: 'a', content: 'Новости и полезная информация'},
              ]},
            ]},
            {block: 'col-12', cls: 'col-lg-6 order-lg-3 mb-3', content: new Array(3).fill(require('./common/card-news_image.bemjson'))},
            {block: 'col-12', cls: 'col-lg-6 order-lg-2', content: [
              {block: 'h', size: '2', content: [
                {block: 'a', content: 'Об аптечной сети'},
              ]},
            ]},
            {block: 'col-12', cls: 'col-lg-6 order-lg-4  mb-3', content: new Array(3).fill(require('./common/card-news.bemjson'))},
          ]},
        ]},
        {block: 'mb-3', content: [
          require('./common/bg-pattern.bemjson'),
        ]},
        {block: 'mb-3', content: [
          {block: 'p-3', cls: 'bg-light rounded', content: [
            require('./common/alphabet.bemjson'),
          ]},
        ]},
        require('./common/navigation-catalog.bemjson'),
      ]},
    ]},
    require('./common/footer.bemjson.js'),
  ],
};
