module.exports = {
  block: 'page',
  title: 'Корзина',
  content: [
    require('./common/header.bemjson.js'),
    {block: 'main', content: [
      {mix: {block: 'container'}, content: [
        require('./common/breadcrumb.bemjson.js'),
        {block: 'd-flex', cls: 'h1 mt-2 justify-content-between align-items-center', content: [
          {block: 'h', size: '1', cls: 'h1 my-0', content: 'Корзина'},
        ]},
        require('./common/basket-list.bemjson.js'),
        {block: 'container-dialog', content: [
          {block: 'h', size: 3, cls: 'text-primary', content: 'Контактные данные'},
          {block: 'form-group', cls: 'row align-items-baseline', content: [
            {tag: 'label', cls: 'col-sm-4 col-md-2 col-form-label-up-sm required', content: 'Ваше имя'},
            {block: 'col-12', cls: 'col-sm-8 col-md-10', content: [
              {block: 'form-control', cls: 'form-control-lg bg-light', attrs: {required: true, placeholder: 'Имя'}},
            ]},
          ]},
          {block: 'form-group', cls: 'row  align-items-baseline', content: [
            {tag: 'label', cls: 'col-sm-4 col-md-2 col-form-label-up-sm required', content: 'Ваш номер'},
            {block: 'col-12', cls: 'col-sm-8 col-md-10', content: [
              {block: 'form-control', cls: 'form-control-lg bg-light', attrs: {required: true, placeholder: '+7 (xxx) xxx-xx-xx', type: 'tel'}},
            ]},
          ]},
          {block: 'form-group', cls: 'row align-items-baseline', content: [
            {tag: 'label', cls: 'col-sm-4 col-md-2 col-form-label-up-sm required', content: 'Ваш email'},
            {block: 'col-12', cls: 'col-sm-8 col-md-10', content: [
              {block: 'form-control', cls: 'form-control-lg bg-light', attrs: {required: true, placeholder: 'Введите email ', type: 'email'}},
            ]},
          ]},
          {block: 'h', size: 3, cls: 'text-primary', content: 'Регион доставки'},
          {block: 'form-group', content: [
            ['Волгоград'].map((name, index)=>{
              return {block: 'form-check', cls: 'form-check-inline align-middle', type: 'radio', checked: !index, name: 'REGION', content: name};
            }),
            {block: 'a', mods: {underline: 'dashed'}, attrs: {'href': '#MODAL_LOCATION', 'data-toggle': 'modal'}, cls: 'align-middle text-nowrap', content: 'Выбрать другой регион'},
          ]},
          {block: 'h', size: 3, cls: 'text-primary', content: 'Доставка'},
          {block: 'form-group', content: [
            ['Самовывоз'].map((name, index)=>{
              return {block: 'form-check', cls: 'form-check-inline', checked: !index, type: 'radio', name: 'DELIVERY', content: name};
            }),
          ]},
        ]},
        {block: 'map', attrs: {'data-points': 'stubs/map-shops.json'}, content: [
          {elem: 'inner'},
          {elem: 'dialog', content: [
            {elem: 'container', content: [
              {elem: 'pane'},
            ]},
          ]},
        ]},
        {block: 'container-dialog', content: [
          {block: 'h', size: 3, cls: 'text-primary', content: 'Оплата'},
          {block: 'form-group', content: [
            ['Наличными'].map((name, index)=>{
              return {block: 'form-check', cls: 'form-check-inline', checked: !index, type: 'radio', name: 'PAY', content: name};
            }),
          ]},
          {block: 'h', size: 3, cls: 'text-primary', content: 'Комментарий'},
          {block: 'form-group', content: [
            {block: 'form-control', cls: 'form-control-lg', tag: 'textarea', attrs: {rows: 7}},
          ]},
          {block: 'form-check', content: 'Нажимая на кнопку «Оформить заказ», я даю свое согласие на обработку моих персональных данных'},
          {block: 'text-center', cls: 'mt-3', content: [
            {block: 'btn', cls: 'btn-gradient-primary', content: 'Оформить заказ'},
          ]},
        ]},
      ]},
    ]},
    require('./common/footer.bemjson.js'),
  ],
};
