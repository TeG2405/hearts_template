module.exports = {
  block: 'page',
  title: 'Детальная странциа каталога',
  content: [
    require('./common/header.bemjson.js'),
    {block: 'main', content: [
      {mix: {block: 'container'}, content: [
        require('./common/breadcrumb.bemjson.js'),
        {block: 'd-flex', cls: 'h1 mt-2 justify-content-between align-items-center', content: [
          {block: 'h', size: '1', cls: 'h1 my-0', content: 'Наименование товара'},
        ]},
        require('./common/product.bemjson'),
        {block: 'mb-3', content: [
          {tag: 'p', content: 'Грипп (фр. grippe) — острое инфекционное заболевание дыхательных путей, вызываемое вирусом гриппа. Входит в группу острых респираторных вирусных инфекций (ОРВИ). Периодически распространяется в виде эпидемий и пандемий. В настоящее время выявлено более 2000 вариантов вируса гриппа, различающихся между собой антигенным спектром[1]. По оценкам ВОЗ, от всех вариантов вируса во время сезонных эпидемий в мире ежегодно умирают от 250 до 500 тыс. человек (большинство из них старше 65 лет), в некоторые годы число смертей может достигать миллиона[2].'},
          {tag: 'p', content: 'Нередко словом «грипп» в обиходе также называют любое острое респираторное заболевание (ОРВИ), что ошибочно, так как кроме гриппа на сегодняшний день описано ещй более 200 видов других респираторных вирусов (аденовирусы, риновирусы, респираторно-синцитиальные вирусы и др.), вызывающих гриппоподобные заболевания у человека[3].'},
          {tag: 'p', content: 'Для профилактики гриппа Центры по контролю и профилактике заболеваний США рекомендуют вакцинировать всех лиц старше 6 месяцев (особенно входящих в группы риска), применять средства индивидуальной защиты, сократить контакты с заболевшими, применять противовирусные препараты по назначению врача[4].'},
        ]},
        {block: 'mb-3', content: [
          {block: 'h', size: 3, cls: 'text-center', content: 'Вы недавно смотрели'},
          require('./common/swiper-row-product.bemjson'),
        ]},
        {block: 'mb-3', content: [
          {block: 'p-3', cls: 'bg-light rounded', content: [
            require('./common/alphabet.bemjson'),
          ]},
        ]},
        require('./common/navigation-catalog.bemjson'),
      ]},
    ]},
    require('./common/footer.bemjson.js'),
  ],
};
