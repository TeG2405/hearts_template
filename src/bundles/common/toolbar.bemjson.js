module.exports = [
  {block: 'toolbar', content: [
    {elem: 'container', cls: 'container', content: [
      {block: 'nav', mods: {inline: true}, content: [
        {elem: 'item', content: [
          {elem: 'link', cls: 'text-primary', attrs: {'data-toggle': 'modal', 'href': '#MODAL_LOCATION'}, content: [
            {block: 'fi', cls: 'pr-1 align-middle', mods: {icon: 'mark'}},
            {tag: 'span', cls: 'align-middle', content: 'Петропавловск-Камчатскиий'},
          ]},
        ]},
        {elem: 'item', content: [
          {elem: 'link', content: [
            {tag: 'span', cls: 'align-middle', content: 'Все аптеки (99)'},
          ]},
        ]},
      ]},
      {block: 'nav', mods: {inline: true}, cls: 'd-none d-lg-flex', content: [
        'Доставка и оплата',
        'Акции',
        'Задать вопрос',
      ]},
      {block: 'nav', mods: {inline: true}, content: [
        {elem: 'item', content: [
          {elem: 'link', content: [
            {block: 'fi', cls: 'pr-1 align-middle text-large', mods: {icon: 'star-o'}},
            {tag: 'span', cls: 'align-middle', content: 'Избранное'},
          ]},
        ]},
        {elem: 'item', content: [
          {elem: 'link', content: [
            {block: 'fi', cls: 'pr-1 align-middle text-large', mods: {icon: 'user-o'}},
            {tag: 'span', cls: 'align-middle', content: 'Войти'},
          ]},
        ]},
      ]},
    ]},
  ]},
];
