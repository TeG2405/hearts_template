module.exports = [
  {block: 'search', content: [
    {tag: 'form', attrs: {action: '/'}, content: [
      {elem: 'inner', content: [
        {elem: 'label', content: [
          {elem: 'input'},
        ]},
        {elem: 'control', cls: 'btn-gradient-primary', content: [
          {block: 'fi', mods: {icon: 'search'}},
        ]},
      ]},
    ]},
  ]},
];
