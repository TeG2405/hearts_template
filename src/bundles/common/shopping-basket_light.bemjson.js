module.exports = [
    {block: 'shopping-basket', mods: {light: true}, content: [
        {elem: 'icon', content: [
            {block: 'fi', mods: {icon: 'shopping-basket'}},
        ]},
        {elem: 'inner', content: [
            {tag: 'span', content: '15 товаров'},
            {tag: 'span', content: {tag: 'b', content: '10 027 руб.'}},
        ]},
    ]},
];
