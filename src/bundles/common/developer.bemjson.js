module.exports = [
    {block: 'developer', content: [
        {block: 'a', attrs: {href: 'http://intervolga.ru', target: '_blank'}, content: 'Разработка сайта:'},
        {block: 'mt-1', content: [
            {block: 'img', width: 160, src: 'images/iv-svg.svg'},
        ]},
    ]},
];
