const ITEMS = [
  {title: 'Иресса табл.п.о. 250мг N30', price: '7 980 руб.'},
  {title: 'Иресса табл.п.о. 250мг N30', price: '7 980 руб.'},
  {title: 'Иресса табл.п.о. 250мг N30', price: '80 руб.'},
];
module.exports = [
  {block: 'basket-list', cls: 'mb-4', content: [
    ITEMS.map((item, index)=>{
      return [
        !index && {elem: 'item', elemMods: {remove: true}, cls: 'fade show', content: [
          {elem: 'col', cls: 'order-sm-1 order-lg-1', elemMods: {title: true}, content: [
            {elem: 'text', content: [
              'Товар —  ',
              {tag: 'span', cls: 'mx-1', content: item.title},
              {tag: 'span', cls: 'text-danger', content: 'был удалён из корзины'},
            ]},
          ]},
          {elem: 'col', cls: 'order-sm-4 order-lg-2', content: [
            {elem: 'recovery', content: 'Восстановить'},
          ]},
          {elem: 'col', cls: 'order-sm-2 order-lg-3', content: [
            {elem: 'close', cls: 'h3 m-0', content: [
              {block: 'fi', mods: {icon: 'times'}},
            ]},
          ]},
        ]},
        {elem: 'item', cls: !index ? 'd-none': '', content: [
          {elem: 'col', cls: 'order-sm-2 order-lg-2', elemMods: {title: true}, content: [
            {elem: 'title', cls: 'h3 my-0', content: [
              {elem: 'text', content: item.title},
            ]},
            {elem: 'code', content: '9G200_T3_22'},
          ]},
          {elem: 'col', cls: 'order-sm-1 order-lg-1', content: [
            {elem: 'image', content: [
              {block: 'image-control', mods: {size: '100x100'}, content: [
                {elem: 'inner', content: [
                  {block: 'img', cls: 'img-fluid', src: 'http://placehold.it/100x100'},
                ]},
              ]},
            ]},
          ]},
          {elem: 'col', cls: 'order-sm-3 order-lg-5', content: [
            {elem: 'close', cls: 'h3 m-0', content: [
              {block: 'fi', mods: {icon: 'times'}},
            ]},
          ]},
          {block: 'd-lg-none', cls: 'w-100 order-sm-4'},
          {elem: 'col', cls: 'order-sm-5 order-lg-3', content: [
            {elem: 'count', content: [
              {block: 'count'},
            ]},
          ]},
          {elem: 'col', cls: 'order-sm-6 order-lg-4', content: [
            {elem: 'price', cls: 'h3 my-0', content: [
              {block: 'price', content: item.price},
            ]},
          ]},
        ]},
      ];
    }),
    {elem: 'footer', cls: 'h3 m-0', content: [
      {elem: 'col', content: [
        {tag: 'span', content: 'Самовывоз:'},
        {elem: 'total', cls: 'text-danger', content: 'Бесплатно'},
      ]},
      {elem: 'col', content: [
        {tag: 'span', content: 'Итого:'},
        {elem: 'total', cls: 'text-danger', content: '1 000 000 руб.'},
      ]},
    ]},
  ]},
];
