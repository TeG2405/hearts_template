module.exports = [
    {block: 'search-panel', content: [
        {elem: 'container', cls: 'container', content: [
            {elem: 'row', cls: 'row', content: [
                {elem: 'col', cls: 'col', content: [
                    {elem: 'duplicate', content: [
                        require('./search.bemjson'),
                    ]},
                    {elem: 'inner', content: [
                        {block: 'h', size: '5', cls: 'mt-0 mb-2', content: 'Категории'},
                        {block: 'list', cls: 'list-unstyled', content: [
                            '<a href="#">Жаропонижающие средства</a>',
                            '<a href="#">Антиагрегантные средства</a>',
                        ]},
                        {mix: {block: 'custom-scroll-bar', mods: {variant: 'white'}}, content: [
                            {elem: 'list', cls: 'pl-2', content: [
                                new Array(10).fill(require('./card-product-row.bemjson')),
                            ]},
                        ]},
                    ]},
                ]},
            ]},
        ]},
    ]},
];
