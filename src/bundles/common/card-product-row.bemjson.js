module.exports = [
    {block: 'card-product', mods: {variant: 'row'}, cls: 'text-left', content: [
        {elem: 'favorite', attrs: {onclick: 'this.classList.toggle("active"); return false;'}, content: [
            {block: 'fi', mods: {icon: 'star-o'}},
        ]},
        {elem: 'row', content: [
            {elem: 'col', cls: 'd-none d-sm-block flex-grow-0', content: [
                {elem: 'link', content: [
                    {elem: 'image', content: [
                        {block: 'image', mods: {size: '120x120', variant: 'shadow'}},
                    ]},
                ]},
            ]},
            {elem: 'col', content: [
                {elem: 'link', content: [
                    {mix: {block: 'mb-1'}, content: [
                        {elem: 'title', cls: 'h4', content: 'Альфа Нормикс 0,2 N12'},
                        {elem: 'subtitle', content: 'Санофи-Авентис, Франция'},
                    ]},
                ]},
                {elem: 'control', content: [
                    {elem: 'col', cls: 'text-left flex-grow-0', content: [
                        {elem: 'price'},
                    ]},
                    {elem: 'col', content: [
                        {block: 'count', cls: 'bg-transparent'},
                    ]},
                    {elem: 'col', content: [
                        {block: 'btn', cls: 'btn-gradient-primary btn-block', content: 'Купить'},
                    ]},
                ]},
            ]},
        ]},
    ]},
];
