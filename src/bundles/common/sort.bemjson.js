module.exports = [
    {block: 'sort', cls: 'dropdown', content: [
        {elem: 'control', cls: 'dropdown-toggle', attrs: {'data-toggle': 'dropdown', 'data-label': 'Сортировать по: '}, content: {tag: 'span', content: 'Цена по возрастанию'}},
        {block: 'dropdown-menu', content: [
            {elem: 'item', cls: 'active', content: 'Цена по возрастанию'},
            'Цена по убыванию',
            {elem: 'divider', cls: 'my-0'},
            'Характеристика по убыванию',
            'Характеристика по возрастанию',
            {elem: 'divider', cls: 'my-0'},
            'Характеристика по убыванию',
            'Характеристика по возрастанию',
        ]},
    ]},
];