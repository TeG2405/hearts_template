module.exports = [
    {block: 'bg-pattern', cls: 'bg-primary text-white rounded', content: [
        {block: 'row', cls: 'align-items-center', content: [
            {block: 'col-12', cls: 'col-lg-6  d-none d-lg-flex align-items-end', content: [
                {block: 'pt-3', content: [
                    {block: 'img', cls: 'img-fluid', src: 'upload/img-1.png'},
                ]},
            ]},
            {block: 'col-12', cls: 'col-lg-6 py-3 pr-3', content: [
                {block: 'h', size: '2', content: 'Дисконтная программа'},
                {tag: 'p', content: 'Для того, чтобы стать участником дисконтной программы, необходимо заполнить анкету в любой аптеке сети, или зарегистрироваться на сайте <a class="text-white" href="#">www.site.ru</a>'},
                {tag: 'p', content: 'Скидка 15% появится после предоставления клиентом достоверных контактных данных (верификации телефонного номера). В интернет- магазине Www.site.ru скидка 3% доступна сразу после регистрации в программе, начиная с первой покупки. Дисконтная карта является накопительной: каждая следующая покупка в Сети ведет к большему проценту скидки!'},
            ]},
        ]},
    ]},
];
