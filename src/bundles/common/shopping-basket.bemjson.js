module.exports = [
    {block: 'shopping-basket', content: [
        {elem: 'icon', content: [
            {block: 'fi', mods: {icon: 'shopping-basket'}},
        ]},
        {elem: 'inner', content: [
            {elem: 'title', content: 'Ваша корзина'},
            {tag: 'span', content: '15 товаров'},
            {tag: 'span', content: {tag: 'b', content: '10 027 руб.'}},
        ]},
    ]},
];