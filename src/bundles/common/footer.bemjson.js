module.exports = [
  {block: 'footer', content: [
    {elem: 'container', cls: 'container', content: [
      {elem: 'row', cls: 'row', content: [
        {elem: 'col', cls: 'col-12 col-sm-12 col-lg-4 col-xl-3 pb-2 mb-2 mb-lg-0 pb-lg-0 d-none d-lg-block', content: [
          {elem: 'logo', content: [
            {block: 'image', mods: {size: '265x90', align: 'middle'}, content: [
              {block: 'img', width: 220, src: 'images/logo.png'},
            ]},
          ]},
          {elem: 'phone', content: [
            {block: 'a', href: 'tel:880070020000', content: '8 (800) 700 200 00'},
          ]},
          {block: 'a', href: 'mailto:info@sr.farm', content: 'info@sr.farm'},
        ]},
        {elem: 'col', cls: 'col-12 col-lg-8 col-xl-9', content: [
          {elem: 'columns', content: [
            {block: 'nav-panel', breakpoint: 'lg', content: [
              {elem: 'title', content: [
                {block: 'a', content: 'О нас'},
                {elem: 'control'},
              ]},
              {elem: 'collapse', content: [
                {elem: 'list', content: [
                  {block: 'a', content: 'Адреса и реквизиты'},
                  {block: 'a', content: 'Производители'},
                  {block: 'a', content: 'Миссия и ценности'},
                ]},
              ]},
            ]},
            {block: 'nav-panel', breakpoint: 'lg', content: [
              {elem: 'title', content: [
                {block: 'a', content: 'Партнерам'},
                {elem: 'control'},
              ]},
              {elem: 'collapse', content: [
                {elem: 'list', content: [
                  {block: 'a', content: 'Размещение рекламы'},
                  {block: 'a', content: 'Расширение сети'},
                ]},
              ]},
            ]},
            {block: 'nav-panel', breakpoint: 'lg', content: [
              {elem: 'title', content: [
                {block: 'a', content: 'Задать вопрос'},
                {elem: 'control'},
              ]},
              {elem: 'collapse', content: [
                {elem: 'list', content: [
                  {block: 'a', content: 'Частые вопросы'},
                ]},
              ]},
            ]},
            {block: 'nav-panel', breakpoint: 'lg', content: [
              {elem: 'title', content: [
                {block: 'a', content: 'Доставка и оплата'},
                {elem: 'control'},
              ]},
              {elem: 'collapse', content: [
                {elem: 'list', content: [
                  {block: 'a', content: 'Где забрать мой заказ?'},
                  {block: 'a', content: 'Как оплатить?'},
                  {block: 'a', content: 'Когда я получу заказ?'},
                ]},
              ]},
            ]},
          ]},
        ]},
      ]},
      {elem: 'row', cls: 'row mt-lg-3', content: [
        {elem: 'col', cls: 'col-12', content: [
          {elem: 'row', cls: 'row', content: [
            {elem: 'col', cls: 'col-12 col-lg-8 col-xl-12', content: [
              require('./subscribe.bemjson'),
            ]},
            {elem: 'col', cls: 'col-12 col-lg-4 col-xl-12 mt-2 mt-lg-0 d-xl-none', content: [
              require('./developer.bemjson'),
            ]},
          ]},
        ]},
      ]},
    ]},
  ]},
  require('./modal-location.bemjson'),
];
