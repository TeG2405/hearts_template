const HEADERS = [
    'Лекарства и БАДы',
    'Спорт и фитнес',
    'Гигиена',
    'Косметика',
    'Мама и малыш',
    'Линзы',
    'Мед. приборы',
];
const DATA = [
    'Дерматология',
    'Лекарственные травы',
    'Лекраства для Глаз',
    'Для потенции',
    'Гомеопатия',
    'Бумага туалетная',
    'Антибактериальные средства',
    'Средства защиты от насекомых',
    'Средства женской гигиены',
    'Расходные средства гигиены',
    'Дерматология',
    'Лекарственные травы',
    'Лекраства для Глаз',
    'Для потенции',
    'Гомеопатия',
];
module.exports = [
    {block: 'navigation-catalog', content: HEADERS.map((item)=>[
        {block: 'nav-panel', breakpoint: 'lg', content: [
            {elem: 'title', cls: 'h4 text-primary', content: [
                {block: 'a', content: item},
                {elem: 'control'},
            ]},
            {elem: 'collapse', content: [
                {elem: 'list', content: new Array(Math.floor(Math.random() * DATA.length) + 2).fill(null).map(()=>[
                    {block: 'a', content: DATA[Math.floor(Math.random() * DATA.length)]},
                ])},
            ]},
        ]},
    ])},
];
