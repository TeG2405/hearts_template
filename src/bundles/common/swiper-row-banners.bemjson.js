module.exports = [
    {block: 'row', content: [
        {block: 'swiper-row', cls: 'swiper-container swiper-container-horizontal w-100', content: [
            {block: 'swiper-wrapper', content: ((item) => new Array(12).fill(item))([
                {block: 'col-12', cls: 'swiper-slide col-md-6', content: [
                    {block: 'image', mods: {size: '600x340'}, content: [
                        {elem: 'inner', content: [
                            {block: 'img', cls: 'rounded', mods: {lazy: true}, src: 'http://placehold.it/600x340'},
                        ]},
                    ]},
                ]},
            ])},
            {elem: 'button', cls: 'btn-gradient-primary', mods: {prev: true}, content: {block: 'fi', mods: {icon: 'angle-left'}}},
            {elem: 'button', cls: 'btn-gradient-primary', mods: {next: true}, content: {block: 'fi', mods: {icon: 'angle-right'}}},
        ]},
    ]},
];
