module.exports = [
  {block: 'header', content: [
    require('./toolbar.bemjson'),
    {elem: 'container', cls: 'container', content: [
      {elem: 'row', cls: 'row justify-content-between', content: [
        {elem: 'col', cls: 'col-12 col-sm-6 col-lg-4 col-xl-3 align-self-end', content: [
          {elem: 'logo', cls: 'pb-1', content: [
            {block: 'image', mods: {size: '265x90', align: 'middle'}, content: [
              {block: 'img', src: 'images/logo.png'},
            ]},
          ]},
        ]},
        {elem: 'col', cls: 'col-12 col-sm-6 col-lg-3 col-xl-2 order-lg-2 align-self-end d-none d-sm-flex', content: [
          require('./shopping-basket.bemjson'),
        ]},
        {elem: 'col', cls: 'col-12 col-lg-5 d-xl-none', content: [
          {elem: 'dublicate', content: [
            {elem: 'row', cls: 'row d-xl-none', content: [
              {elem: 'col', cls: 'col-12 mb-2 mb-lg-0', content: [
                {elem: 'phone', content: [
                  {block: 'a', href: 'tel:880070020000', content: '8 (800) 700 200 00'},
                  {tag: 'small', content: 'Звонок по России бесплатныий'},
                ]},
              ]},
              {elem: 'col', cls: 'col-12', content: [
                {elem: 'snippet', content: 'Вы сможете забрать любой заказ на следующий день'},
              ]},
            ]},
          ]},
        ]},
        {elem: 'col', cls: 'col-12 col-xl-7 order-lg-2 order-xl-1', content: [
          {elem: 'row', cls: 'row d-none d-xl-flex', content: [
            {elem: 'col', cls: 'px-2', content: [
              {elem: 'phone', cls: 'pl-xl-2', content: [
                {block: 'a', href: 'tel:880070020000', content: '8 (800) 700 200 00'},
                {tag: 'small', content: 'Звонок по России бесплатныий'},
              ]},
            ]},
            {elem: 'col', cls: 'col', content: [
              {elem: 'snippet', content: 'Вы сможете забрать любой заказ на следующий день'},
            ]},
          ]},
          {elem: 'row', cls: 'row mt-2', content: [
            {elem: 'col', cls: 'col-12', content: [
              require('./search.bemjson'),
            ]},
          ]},
        ]},
      ]},
      {block: 'fixed-top-control', cls: 'position-relative', content: [
        require('./search-panel.bemjson'),
      ]},
    ]},
    {block: 'fixed-top-control', content: [
      require('./navigation.bemjson'),
    ]},
  ]},
];
