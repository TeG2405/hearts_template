module.exports = [
    {block: 'navigation', content: [
        {elem: 'container', mix: {block: 'container'}, content: [
            {elem: 'inner', content: [
                {elem: 'list', content: [
                    {elem: 'item', content: [
                        {elem: 'link', elemMods: {control: true}, cls: 'px-3', attrs: {'data-toggle': 'open'}, content: [
                            {block: 'fi', mods: {icon: 'bar'}},
                            {tag: 'span', cls: 'd-none d-lg-inline ml-1', content: 'Каталог'},
                            {block: 'fi', cls: 'd-none d-lg-inline ml-1', mods: {icon: 'angle-down'}},
                        ]},
                        require('./navigation-panel.bemjson'),
                    ]},
                ]},
                {elem: 'duplicate', content: [
                    {elem: 'overflow', content: [
                        {elem: 'list', cls: 'd-none d-sm-flex', content: [
                            'Лекарства и БАДы',
                            'Гигиена',
                            'Мама и малыш',
                            'Мед. приборы',
                            'Спорт и фитнес',
                            'Косметика',
                            'Линзы',
                        ]},
                    ]},
                    {elem: 'supernatant', content: [
                        {block: 'w-100', content: [
                            {block: 'row', cls: 'justify-content-end align-items-center', content: [
                                {block: 'col', cls: 'd-none d-sm-block', content: [
                                    require('./search.bemjson'),
                                ]},
                                require('./shopping-basket_light.bemjson'),
                            ]},
                        ]},
                    ]},
                ]},
            ]},
        ]},
    ]},
];
