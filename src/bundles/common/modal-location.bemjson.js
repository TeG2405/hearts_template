module.exports = [
    {block: 'modal', cls: 'fade', attrs: {id: 'MODAL_LOCATION'}, content: [
        {elem: 'dialog', cls: 'modal-lg', content: [
            {elem: 'content', content: [
                {elem: 'header', content: [
                    {elem: 'title', content: 'Выбирите ваш город из списка'},
                    {elem: 'close'},
                ]},
                {elem: 'body', content: [
                    {block: 'form-control', attrs: {placeholder: 'Начните вводить название города'}},
                ]},
                {elem: 'footer', content: [
                    {block: 'btn', attrs: {'data-dismiss': 'modal'}, cls: 'btn-secondary', content: 'Закрыть'},
                    {block: 'btn', content: 'Сохранить'},
                ]},
            ]},
        ]},
    ]},
];
