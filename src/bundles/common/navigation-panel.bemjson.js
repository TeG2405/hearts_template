const HEADERS = [
    'Лекарства и БАДы',
    'Спорт и фитнес',
    'Гигиена',
    'Косметика',
    'Мама и малыш',
    'Линзы',
    'Мед. приборы',
];
const DATA = [
    'Дерматология',
    'Лекарственные травы',
    'Лекраства для Глаз',
    'Для потенции',
    'Гомеопатия',
    'Бумага туалетная',
    'Антибактериальные средства',
    'Средства защиты от насекомых',
    'Средства женской гигиены',
    'Расходные средства гигиены',
    'Дерматология',
    'Лекарственные травы',
    'Лекраства для Глаз',
    'Для потенции',
    'Гомеопатия',
];
module.exports = [
    {block: 'navigation-panel', content: [
        {elem: 'inner', content: [
            {mix: {block: 'navigation-panel', elem: 'item'}, content: [
                {elem: 'title', content: 'Каталог'},
                {elem: 'control', content: [
                    {block: 'fi', mods: {icon: 'times'}},
                ]},
            ]},
            HEADERS.map((item)=>[
                {elem: 'item', content: [
                    {elem: 'link', content: item},
                    {elem: 'control', content: [
                        {block: 'fi', mods: {icon: 'angle-right'}},
                    ]},
                    {elem: 'dropdown', content: [
                        {mix: {block: 'navigation-panel', elem: 'item'}, content: [
                            {elem: 'title', content: item},
                            {elem: 'control', content: [
                                {block: 'fi', mods: {icon: 'angle-left'}},
                            ]},
                        ]},
                        {elem: 'list', content: new Array(Math.floor(Math.random() * DATA.length) + 2).fill(null).map(() => DATA[Math.floor(Math.random() * DATA.length)])},
                    ]},
                ]},
            ]),
            {elem: 'expanded', content: [
                {elem: 'item', content: [
                    {elem: 'link', content: 'Доставка и оплата'},
                ]},
                {elem: 'item', content: [
                    {elem: 'link', content: 'Акции'},
                ]},
                {elem: 'item', content: [
                    {elem: 'link', content: 'Задать вопрос'},
                ]},
            ]},
        ]},
    ]},
];

