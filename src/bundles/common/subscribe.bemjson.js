module.exports = [
    {block: 'subscribe', content: [
        {elem: 'row', cls: 'row align-items-center', content: [
            {elem: 'col', cls: 'col-12 col-lg-6 col-xl-3', content: [
                {elem: 'title', cls: 'h4', content: 'Подпишитесь и получайте наши предложения первыми'},
            ]},
            {elem: 'col', cls: 'col-12 col-lg-6 col-xl-4', content: [
                {elem: 'inner', content: [
                    {elem: 'label', content: [
                        {elem: 'input'},
                    ]},
                    {elem: 'control', cls: 'btn-gradient-primary', content: 'ОК'},
                ]},
            ]},
            {elem: 'col', cls: 'col-12 col-lg-12 col-xl-3', content: [
                {elem: 'description', content: 'Нажимая на кнопку, вы даёте согласие на обработку персональных данных и соглашаетесь с политикой конфиденциальности'},
            ]},
            {elem: 'col', cls: 'd-none d-xl-block col-xl-2', content: [
                require('./developer.bemjson'),
            ]},
        ]},
    ]},
];
