module.exports = [
    {block: 'map', attrs: {'data-points': 'stubs/map-shops.json'}, content: [
        {elem: 'inner'},
        {elem: 'dialog', content: [
            {elem: 'container', content: [
                {elem: 'pane', content: [
                    {elem: 'title', cls: 'pl-2', content: 'Сеть аптек <br> в вашем городе'},
                ]},
            ]},
        ]},
    ]},
];