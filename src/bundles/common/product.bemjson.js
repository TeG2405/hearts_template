module.exports = [
    {block: 'product', content: [
        {elem: 'slider', cls: 'col-12 col-lg-4', content: [
            {cls: 'swiper-container swiper-container-horizontal w-100', content: [
                {block: 'swiper-wrapper', content: new Array(6).fill([
                    {block: 'swiper-slide', content: {block: 'image', mods: {size: '300x300'}}},
                ])},
            ]},
            {elem: 'thumbnails', content: [
                {block: 'row', content: [
                    {cls: 'swiper-container swiper-container-horizontal w-100', content: [
                        {block: 'swiper-wrapper', content: [
                            {block: 'swiper-slide', cls: 'col-4 col-sm-3 col-md-2 col-lg-3 selected', content: {block: 'image', mods: {size: '100x100'}}},
                            new Array(5).fill([
                                {block: 'swiper-slide', cls: 'col-4 col-sm-3 col-md-2 col-lg-3', content: {block: 'image', mods: {size: '100x100'}}},
                            ]),
                        ]},
                    ]},
                ]},
            ]},
            {elem: 'instructions', cls: 'btn btn-light py-2', content: [
                {block: 'fi', cls: 'pr-2', mods: {icon: 'book'}},
                {content: [
                    {cls: 'h6 mb-1 text-primary', content: 'Инструкция по применению'},
                    {block: 'd-flex', cls: 'justify-content-between w-100', content: [
                        {tag: 'small', content: 'Формат документа: PDF'},
                        {tag: 'small', content: 'Размер: 500 КБ'},
                    ]},
                ]},
            ]},
        ]},
        {elem: 'description', cls: 'col-12 col-lg-8', content: [
            {elem: 'well', content: [
                require('./product-panel.bemjson'),
            ]},
            {elem: 'list', content: [
                'Производитель: <a href="#"><b>ЯДРАН</b></a>',
                'Страна происхождения: <b>Хорватия</b>',
                'Форма выпуска: <b>Капли</b>',
                'Действующие вещества: <b>Морская вода</b>',
                'Срок годности: <b>05.04.2018</b>',
            ]},
            {elem: 'well', content: [
                {block: 'h', size: '3', cls: 'text-primary mb-2', content: 'Забрать товар можно <span class="text-danger">завтра</span> по адресу:'},
                {cls: 'row mb-2', content: new Array(2).fill([
                    {cls: 'col-12 col-sm-12 col-md-6 col-lg-12 col-xl-6 my-1', content: [
                        {elem: 'address', content: ['Беломорск, ул. Порт-шоссе, 17', 'Пн—Вс: 9:00—19:00', '+7 (8142) 44-50-95, +7 (900) 458-87-84'].join('<br>')},
                    ]},
                ])},
                {block: 'a', content: 'Все аптеки на карте'},
            ]},
            {block: 'h', size: '3', cls: 'text-primary mb-2', content: 'Характеристики'},
            {elem: 'list', content: [
                'Способ применения: <b>Интраназально</b>',
                'Количество в упаковке: <b>1 шт</b>',
                'Срок годности: <b>24 мес</b>',
                'Максимальная допустимая температура хранения, оС: <b>25о</b>',
                'Условия хранения: <b>Беречь от детей</b>',
                'Форма выпуска: <b>Капли</b>',
                'Объем: <b>10 мл</b>',
                'Срок хранения после открытия: <b>45 суток</b>',
                'Порядок отпуска: <b>Без рецепта</b>',
                'Действующее вещество: <b>Морская вода (Sea water)</b>',
                'Сфера применения: <b>Оториноларингология</b>',
            ]},
        ]},
        {elem: 'analog', cls: 'col-12 col-lg-4', content: [
            {block: 'h', size: '5', cls: 'd-none d-lg-block mb-1', content: 'Аналог <span class="text-primary">"Аква Марис норм спрей 150 мл"</span> по действующему веществу <span class="text-primary">Морская вода</span>'},
            {block: 'h', size: '3', cls: 'text-center d-lg-none', content: 'Аналог <span class="text-primary">"Аква Марис норм спрей 150 мл"</span> по действующему веществу <span class="text-primary">Морская вода</span>'},
            require('./swiper-row-product_analog.bemjson'),
        ]},
    ]},
];
