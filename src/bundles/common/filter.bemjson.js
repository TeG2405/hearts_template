module.exports = [
    {block: 'filter', content: [
        {cls: 'd-none', content: [
            {elem: 'popover', content: [
                {block: 'btn', cls: 'btn-gradient-primary', content: 'Выбранно (10) показать'},
            ]},
        ]},
        {elem: 'item', content: [
            {elem: 'title', content: 'Дозировка/Упаковка'},
            {elem: 'panel', content: new Array(10).fill([
                {block: 'form-check', content: 'Значение характеристики'},
            ])},
        ]},
        {elem: 'item', content: [
            {elem: 'title', content: 'Цена'},
            {elem: 'panel', content: [
                {block: 'range', range: [0, 1500]},
            ]},
        ]},
        {elem: 'item', content: [
            {elem: 'title', content: 'Действующие вещества'},
            {elem: 'panel', content: new Array(10).fill([
                {block: 'form-check', name: 'RADIO', type: 'radio', content: 'Значение характеристики'},
            ])},
        ]},
        {elem: 'item', content: [
            {elem: 'panel', content: [
                {elem: 'control', content: [
                    {block: 'fi', cls: 'align-middle', mods: {icon: 'times'}},
                    {tag: 'span', cls: 'align-middle', content: 'Сбросить всё'},
                ]},
            ]},
        ]},
    ]},
];
