module.exports = [
    {block: 'row', content: [
        {block: 'swiper-row', cls: 'swiper-container swiper-container-horizontal w-100', content: [
            {block: 'swiper-wrapper', content: new Array(6).fill([
                {block: 'col-12', cls: 'swiper-slide col-sm-12 col-md-6 col-xl-4 py-2 h-auto', content: require('./card-product.bemjson')},
            ])},
        ]},
    ]},
];
