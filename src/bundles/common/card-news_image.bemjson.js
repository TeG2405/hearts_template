const text = require('ryba-js');
module.exports = [
    {block: 'card-news', content: [
        {elem: 'title', cls: 'order-1 d-sm-none', content: [
            {block: 'a', content: text()},
        ]},
        {elem: 'wrapper', content: [
            {elem: 'image', cls: 'order-2', content: [
                {block: 'a', content: [
                    {block: 'img', cls: 'rounded', src: 'http://placehold.it/120x120'},
                ]},
            ]},
            {elem: 'inner', content: [
                {elem: 'title', cls: 'order-1 d-none d-sm-block', content: [
                    {block: 'a', content: text()},
                ]},
                {elem: 'body', cls: 'order-3', content: [
                    {tag: 'p', content: text(10)},
                ]},
            ]},
            {elem: 'more'},
        ]},
    ]},
];
