module.exports = [
    {block: 'product-panel', content: [
        {cls: 'd-flex justify-content-between', content: [
            {content: 'Артикул: 01160077'},
            {elem: 'favorite', content: [
                {block: 'fi', cls: 'pr-1', mods: {icon: 'star-o'}},
                'В избранное',
            ]},
        ]},
        {elem: 'row', content: [
            {elem: 'col', content: [
                {elem: 'price'},
            ]},
            {elem: 'col', content: [
                {block: 'count', size: 'lg', cls: 'bg-transparent'},
            ]},
            {elem: 'col', content: [
                {elem: 'row', cls: 'flex-row-reverse', content: [
                    {elem: 'col', content: [
                        {block: 'btn', cls: 'btn-gradient-primary btn-lg btn-block px-xl-3', content: 'Купить в 1 клик'},
                    ]},
                    {elem: 'col', content: [
                        {block: 'btn', cls: 'btn-gradient-danger  btn-lg btn-block px-xl-4', content: 'В корзину'},
                    ]},
                ]},
            ]},
        ]},
    ]},
];