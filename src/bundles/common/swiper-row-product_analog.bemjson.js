module.exports = [
    {block: 'row', content: [
        {block: 'swiper-row', cls: 'swiper-container swiper-container-horizontal w-100', content: [
            {elem: 'pagination', cls: 'swiper-pagination d-none d-lg-block'},
            {block: 'swiper-wrapper', content: new Array(6).fill([
                {block: 'col-12', cls: 'swiper-slide col-sm-12 col-md-6 col-lg-12 py-2 h-auto', content: require('./card-product.bemjson')},
            ])},
            {elem: 'button', cls: 'btn-gradient-primary', mods: {prev: true}, content: {block: 'fi', mods: {icon: 'angle-left'}}},
            {elem: 'button', cls: 'btn-gradient-primary', mods: {next: true}, content: {block: 'fi', mods: {icon: 'angle-right'}}},
        ]},
    ]},
];
