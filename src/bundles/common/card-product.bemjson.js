module.exports = [
    {block: 'card-product', content: [
        {elem: 'favorite', attrs: {onclick: 'this.classList.toggle("active"); return false;'}, content: [
            {block: 'fi', mods: {icon: 'star-o'}},
        ]},
        {elem: 'inner', content: [
            {elem: 'link', content: [
                {mix: {block: 'mb-1'}, content: [
                    {elem: 'title', cls: 'h4', content: 'Альфа Нормикс 0,2 N12'},
                    {elem: 'subtitle', content: 'Санофи-Авентис, Франция'},
                ]},
                {elem: 'image', cls: 'mb-1', content: [
                    {block: 'image', mods: {size: '485x485'}},
                ]},
            ]},
            {cls: 'my-2', content: [
                {elem: 'feature', content: [
                    'Действующее вещество',
                    {content: {block: 'a', content: 'Поливитамины минералы'}},
                ]},
                {elem: 'feature', content: 'Срок годности: до 01.11.2020'},
            ]},
            {elem: 'control', elemMods: {border: 'dotted'}, cls: 'mt-2', content: [
                {elem: 'col', cls: 'text-left flex-grow-0', content: [
                    {elem: 'price'},
                ]},
                {elem: 'col', content: [
                    {block: 'count'},
                ]},
                {elem: 'col', content: [
                    {block: 'btn', cls: 'btn-gradient-primary btn-block', content: 'Купить'},
                ]},
            ]},
        ]},
    ]},
];
