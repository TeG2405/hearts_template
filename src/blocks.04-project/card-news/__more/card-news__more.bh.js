module.exports = function(bh) {
    bh.match('card-news__more', function(ctx, json) {
        ctx.content([
            {block: 'a', attrs: {'data-toggle': 'open', 'href': '#'+ctx.tParam('ID')}, content: [
                {tag: 'span', content: 'Читать далее'},
                {tag: 'span', content: 'Свернуть'},
            ]},
        ]);
    });
};
