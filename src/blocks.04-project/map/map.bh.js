module.exports = function(bh) {
    bh.match('map', function(ctx, json) {
        ctx.attrs({
            'data-points': 'stubs/map.json',
            'id': ctx.generateId(),
        });
    });
};

