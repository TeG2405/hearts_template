module.exports = {
    mustDeps: [
        {block: 'form-check', mods: ['variant']},
        {block: 'custom-scroll-bar'},
        {block: 'card-shop'},
    ],
    shouldDeps: [
        {elem: ['list', 'li', 'dialog', 'icon']},
    ],
};
