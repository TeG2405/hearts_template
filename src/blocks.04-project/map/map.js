(($)=>{
    const CLASSES = {
        INNER: 'map__inner',
        DIALOG: 'map__dialog',
        PANE: 'map__pane',
    };
    const SETTINGS = {
        COLOR: '#d70200',
        API: '//api-maps.yandex.ru/2.1/?lang=ru_RU',
    };
    class Map {
        constructor(block) {
            this.block = block;
            this.init = this.init.bind(this);
            this.script(SETTINGS.API).then(this.init);
        }
        init() {
            const ymaps = global.ymaps;
            const block = this.block;
            const id = this.block.id;
            const inner = $(block).find(`.${CLASSES.INNER}`).get(0);
            const pane = $(block).find(`.${CLASSES.PANE}`).get(0);
                $.ajax(this.block.getAttribute('data-points'), {
                dataType: 'json',
                success: function(json) {
                    let data = json;
                    window.getBH((BH)=>{
                        ymaps.ready(()=>{
                            var iconContentLayout = ymaps.templateLayoutFactory.createClass(BH.apply([
                                {block: 'map', cls: 'text-$[properties.color]-gradient', elem: 'icon', content: [
                                    {block: 'fi', mods: {icon: 'heart'}},
                                ]},
                            ]));
                            // Инициализация карты;
                            var map = new ymaps.Map(inner, {
                                center: data[0].coords,
                                zoom: 10,
                                controls: [],
                            });
                            // Отключаем перетаскивание если зазмер карты мал
                            if (!(block.offsetWidth - pane.offsetWidth > 10)) {
                                map.controls.add('fullscreenControl');
                                map.behaviors.disable('drag');
                                map.container.events.add('fullscreenenter', ()=>{
                                    map.behaviors.enable('drag');
                                });
                                map.container.events.add('fullscreenexit', ()=>{
                                    map.behaviors.disable('drag');
                                });
                            } else {
                                // Управляем отсупами;
                                if (pane) {
                                    map.margin.addArea({
                                        top: 0,
                                        left: '50%',
                                        width: '50%',
                                        height: '100%',
                                    });
                                }
                            }


                            // Добавление точек на карту;
                            var placemarkList = data.map((item, index)=>{
                                let placemark = new ymaps.Placemark(item.coords,
                                    {
                                        balloonContent: item.balloon && BH.apply([
                                            {block: 'card-shop', tag: item.balloon.link ? 'a': 'div', mods: {light: true}, attrs: {href: item.balloon.link}, content: [
                                                {elem: 'body', cls: 'px-0', content: [
                                                    item.balloon.title && {elem: 'title', content: item.balloon.title},
                                                    {elem: 'list', content: [
                                                        item.balloon.address && {elem: 'li', content: `Адрес: <b>${item.balloon.address}</b>`},
                                                        item.balloon.time && {elem: 'li', content: `Режим работы: <b>${item.balloon.time}</b>`},
                                                        item.balloon.phone && {elem: 'li', content: `Телефон:  <b>${item.balloon.phone}</b>`},
                                                    ]},
                                                ]},
                                            ]},
                                        ]),
                                        color: 'primary',
                                    },
                                    {
                                        iconLayout: 'default#imageWithContent',
                                        iconImageOffset: [-18, -12],
                                        iconImageSize: [36, 24],
                                        iconImageHref: '',
                                        iconContentLayout: iconContentLayout,
                                        // iconColor: SETTINGS.COLOR,
                                    });
                                placemark.events.add('click', function(event) {
                                    $(document.getElementById(item.coords.join('_'))).prop('checked', true).change();
                                });
                                map.geoObjects.add(placemark);
                                return [item.coords.join(';'), placemark];
                            });
                            if (data.length > 1) {
                                // Формировние списка точек
                                if (pane) {
                                    $(pane).append(BH.apply([
                                        {block: 'custom-scroll-bar', content: [
                                            {block: 'map', elem: 'list', content: data.map((item)=>{
                                                return {elem: 'li', content: [
                                                    {block: 'form-check', mods: {variant: 'mark'}, type: 'radio', name: id, id: item.coords.join('_'), value: item.coords.join(';'), content: item.content.join('<br>')},
                                                ]};
                                            })},
                                        ]},
                                    ]));
                                    // Обработчики change
                                    $(block).on('change', (event) => {
                                        map.setCenter(event.target.value.split(';'), 14, {useMapMargin: pane && (block.offsetWidth - pane.offsetWidth > 10)});
                                        placemarkList.forEach((item)=>{
                                            item[1].properties.set({
                                                color: 'primary',
                                            });
                                        });
                                        placemarkList.filter((item) => item[0] == event.target.value).forEach((item)=>{
                                            item[1].properties.set({
                                                color: 'danger',
                                            });
                                        });
                                    });
                                    // Событие изменения дом дерева;
                                    $(document).trigger('OnDocumentHtmlChanged');
                                }
                                // Выравнивание маштаба и центра карты для отображения всех точек;
                                map.setBounds(map.geoObjects.getBounds(), {useMapMargin: pane && (block.offsetWidth - pane.offsetWidth > 10)});
                            } else {
                                map.setCenter(data[0].coords, 14, {useMapMargin: pane && (block.offsetWidth - pane.offsetWidth > 10)});
                            }
                        });
                    });
                },
            });
        }
        script(url) {
            if (Array.isArray(url)) {
                let self = this;
                let prom = [];
                url.forEach(function(item) {
                    prom.push(self.script(item));
                });
                return Promise.all(prom);
            }

            return new Promise((resolve, reject) => {
                let r = false;
                let t = document.getElementsByTagName('script')[0];
                let s = document.createElement('script');
                s.type = 'text/javascript';
                s.src = url;
                s.async = true;
                s.onload = s.onreadystatechange = function() {
                    if (!r && (!this.readyState || this.readyState === 'complete')) {
                        r = true;
                        resolve(this);
                    }
                };
                s.onerror = s.onabort = reject;
                t.parentNode.insertBefore(s, t);
            });
        }
    }

    const init = function() {
        $('.map:visible').each((index, item)=>{
            if (!$(item).data('map')) {
                var map = new Map(item);
                $(item).data('map', map);
            }
        });
    };
    init();
    $(document)
        .on('OnDocumentHtmlChanged', init)
        .on('shown.bs.modal', '.modal', init)
        .on('shown.bs.tab', '[data-toggle="tab"]', init);
})($);

