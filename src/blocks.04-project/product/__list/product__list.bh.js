module.exports = function(bh) {
    bh.match('product__list', function(ctx, json) {
        let content = ctx.tag('ul').content();
        if (Array.isArray(content)) {
            ctx.content(content.map((item) => [
                {tag: 'li', content: item},
            ]), true);
        }
    });
};
