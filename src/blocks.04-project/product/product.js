import Swiper from 'swiper/dist/js/swiper.min';
(($) => {
    Array.prototype.forEach.call(document.getElementsByClassName('product'), (elem) => {
        Array.prototype.forEach.call(elem.getElementsByClassName('product__slider'), (slider) => {
            let main = slider.getElementsByClassName('swiper-container')[0];
            let thumbnail = slider.getElementsByClassName('swiper-container')[1];
            let api = new Swiper(main, {
                slidesPerView: 'auto',
                spaceBetween: 0,
                on: {
                    slideChangeTransitionEnd: () => {
                        thumbnail.getElementsByClassName('selected')[0].classList.remove('selected');
                        thumbnail.getElementsByClassName('swiper-slide')[api.snapIndex].classList.add('selected');
                    },
                },
            });
            new Swiper(thumbnail, {
                slidesPerView: 'auto',
                spaceBetween: 0,
                watchSlidesVisibility: true,
            });
            $(thumbnail).on('mouseenter', '.swiper-slide', (e) => {
                api.slideTo(Array.from(thumbnail.getElementsByClassName('swiper-slide')).indexOf(e.currentTarget));
            });
        });
    });
})($);
