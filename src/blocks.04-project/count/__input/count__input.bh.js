module.exports = function(bh) {
    bh.match('count__input', function(ctx, json) {
        ctx
            .tag('input')
            .attrs({
                value: 1,
                placeholder: 1,
            });
    });
};
