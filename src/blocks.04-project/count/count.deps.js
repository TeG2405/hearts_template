module.exports = {
    mustDeps: [
        {block: 'fi'},
    ],
    shouldDeps: [
        {elem: ['input', 'btn']},
    ],
};
