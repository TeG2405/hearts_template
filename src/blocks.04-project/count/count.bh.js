module.exports = function(bh) {
    bh.match('count', function(ctx, json) {
        if (json.size == 'lg') {
            ctx.mix({block: 'form-control-lg'});
        }
        ctx
            .mix({block: 'form-control'})
            .content([
                {elem: 'btn', cls: 'btn btn-sm p-0 btn-gradient-primary', content: [
                    {block: 'fi', mods: {icon: 'minus'}},
                ]},
                {elem: 'input'},
                {elem: 'btn', cls: 'btn btn-sm p-0 btn-gradient-primary', content: [
                    {block: 'fi', mods: {icon: 'plus'}},
                ]},
            ]);
    });
};
