(($)=>{
    $(document)
        .on('click', '.count__btn', (e)=>{
            let $this = $(e.currentTarget);
            let $block = $this.closest('.count');
            let $input = $block.find('.count__input');
            let min = +$input.attr('placeholder');
            if ($this.find('.fi_icon_minus').length && Number($input.val()) > min) {
                $input.val(Number($input.val()) - 1);
            }
            if ($this.find('.fi_icon_plus').length) {
                $input.val(Number($input.val()) + 1);
            }
        })
        .on('keyup', '.count__input', (e)=>{
            e.target.value = e.target.value > 0 ? e.target.value : +e.target.placeholder;
        });
})($);
