module.exports = function(bh) {
    bh.match('product-panel__price', function(ctx, json) {
        let price = Math.floor(Math.random()*10000);
        ctx.content([
            {tag: 'span', content: [
                {tag: 'del', content: price+' р.'},
                {tag: 'small', block: 'badge', cls: 'badge-pill', content: 'Акция 1+1'},
            ]},
            {block: 'text-primary', content: (price - Math.floor(price / 100 * Math.random()*100))+' р.'},
        ]);
    });
};
