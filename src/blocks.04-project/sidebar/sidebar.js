const Sidebar = (()=>{
    const NAV_HEIGHT = 65;
    class Sidebar {
        constructor(className) {
            this.elems = document.getElementsByClassName(className);
            this.pageYOffset = window.pageYOffset;
            window.addEventListener('scroll', this.check.bind(this));
        }
        check(event) {
            let pageYOffsetOld = this.pageYOffset;
            Array.prototype.forEach.call(this.elems, (elem) => {
                let offsetTopWrapper = Sidebar.offset(elem.parentNode).top;
                if (offsetTopWrapper < window.pageYOffset + NAV_HEIGHT) {
                    if (offsetTopWrapper + elem.parentNode.offsetHeight > window.pageYOffset + window.innerHeight) {
                        elem.scrollTop = elem.scrollTop + (window.pageYOffset - pageYOffsetOld) * 2;
                    } else {
                        elem.scrollTop = elem.parentNode.offsetHeight;
                    }
                } else {
                    elem.scrollTop = 0;
                }
            });
            this.pageYOffset = window.pageYOffset;
        }
        static offset(elem) {
            if (!elem.getClientRects().length) {
                return {top: 0, left: 0};
            }
            let rect = elem.getBoundingClientRect();
            let win = elem.ownerDocument.defaultView;
            return {
                top: rect.top + win.pageYOffset,
                left: rect.left + win.pageXOffset,
            };
        }
    }
    new Sidebar('sidebar');
    return Sidebar;
})();
export default Sidebar;
