module.exports = function(bh) {
    bh.match('filter__panel', function(ctx, json) {
        ctx.cls(ctx.isFirst() ? '' : 'collapse').attrs({
            id: ctx.tParam('ID'),
        });
    });
};
