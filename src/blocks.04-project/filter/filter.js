import {Util} from 'bootstrap';
(($) => {
    $('.filter')
        .on('change', (event) => {
            event.target.closest('.filter__item').append(event.currentTarget.getElementsByClassName('filter__popover')[0]);
        })
        .on('click', '.filter__popover .btn', (event) => {
            let backdrop = document.createElement('div');
            backdrop.classList.add('progress-updated');
            backdrop.classList.add('fade');
            $('.catalog').append(backdrop);
            Util.reflow(backdrop);
            backdrop.classList.add('show');
            setTimeout(() => {
                backdrop.classList.remove('show');
                $(backdrop)
                    .one(Util.TRANSITION_END, () => {
                        backdrop.remove();
                    })
                    .emulateTransitionEnd(Util.getTransitionDurationFromElement(backdrop));
            }, 1500);
            event.preventDefault();
        });
})($);
