module.exports = function(bh) {
    bh.match('subscribe__label', function(ctx, json) {
        ctx
            .tag('label')
            .tParam('ID', ctx.generateId(), true)
            .attrs({
                for: ctx.tParam('ID'),
            });
    });
};
