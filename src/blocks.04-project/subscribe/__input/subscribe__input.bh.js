module.exports = function(bh) {
    bh.match('subscribe__input', function(ctx, json) {
        ctx
            .tag('input')
            .attrs({
                type: 'email',
                name: ctx.tParam('ID'),
                id: ctx.tParam('ID'),
                placeholder: 'Ваш Email',
            });
    });
};
