module.exports = function(bh) {
    bh.match('basket-list__item', function(ctx, json) {
        ctx.attrs({
            'data-product': ctx.generateId(),
        });
    });
};
