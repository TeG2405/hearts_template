module.exports = function(bh) {
    bh.match('basket-list__close', function(ctx, json) {
        ctx.tag('button').attrs({
            type: 'button',
        });
    });
};
