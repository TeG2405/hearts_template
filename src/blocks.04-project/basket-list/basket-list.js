import {Util} from 'bootstrap';
(($)=>{
    $(document)
        .on('click', '.basket-list .basket-list__close', function(e) {
            let $this = $(this);
            let $block = $this.closest('.basket-list');
            let $target = $this.closest('.basket-list__item');
            let product = $target.data('product');
            let TransitionDuration = Util.getTransitionDurationFromElement($target);
            if ($target.hasClass('basket-list__item_remove')) {
                $target.removeClass('show');
                $target
                    .one(Util.TRANSITION_END, ()=>{
                        $target.next('.d-none').remove();
                        $target.remove();
                        $block.trigger(new $.Event('product.block.basket', {action: 'remove', product}));
                    })
                    .emulateTransitionEnd(TransitionDuration);
            } else {
                window.getBH((BH)=>{
                    $target
                        .addClass('d-none')
                        .before(BH.apply([
                            {block: 'basket-list', elem: 'item', elemMods: {remove: true}, attrs: {'data-product': product}, cls: 'fade show', content: [
                                {elem: 'col', cls: 'order-sm-1 order-lg-1', elemMods: {title: true}, content: [
                                    {elem: 'text', content: [
                                        'Товар —  ',
                                        {tag: 'span', cls: 'mx-1', content: $target.find('.basket-list__text').text() + $target.find('.basket-list__badge').text()},
                                        {tag: 'span', cls: 'text-danger', content: 'был удалён из корзины'},
                                    ]},
                                ]},
                                {elem: 'col', cls: 'order-sm-4 order-lg-2', content: [
                                    {elem: 'recovery', tag: 'button', attrs: {type: 'button'}, content: 'Восстановить'},
                                ]},
                                {elem: 'col', cls: 'order-sm-2 order-lg-3', content: [
                                    {elem: 'close', tag: 'button', attrs: {type: 'button'}, cls: 'h3 m-0', content: [
                                        {block: 'fi', mods: {icon: 'times'}},
                                    ]},
                                ]},
                            ]},
                        ]));
                    $block.trigger(new $.Event('product.block.basket', {action: 'remove', product}));
                });
            }
        })
        .on('click', '.basket-list .basket-list__recovery', function(e) {
            let $this = $(e.currentTarget);
            let $block = $this.closest('.basket-list');
            let $target = $this.closest('.basket-list__item');
            let product = $target.next().data('product');
            $target.next().removeClass('d-none');
            $target.remove();
            $block.trigger(new $.Event('product.block.basket', {action: 'recovery', product}));
        });
})($);


