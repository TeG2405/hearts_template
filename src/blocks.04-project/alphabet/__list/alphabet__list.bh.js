module.exports = function(bh) {
    bh.match('alphabet__list', function(ctx, json) {
        ctx.tag('ul').content(
            ctx.content().map((item)=>{
                return ctx.isSimple(item) ? {elem: 'li', content: {block: 'a', content: item}} : item;
            }),
            true
        );
    });
};
