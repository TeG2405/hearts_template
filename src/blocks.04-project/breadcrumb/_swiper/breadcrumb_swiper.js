import Swiper from 'swiper/dist/js/swiper.min';
$('.breadcrumb_swiper').each(function() {
    let $this = $(this);
    let delay = $this.data('delay');
    let api = new Swiper(this, {
        slidesPerView: 'auto',
        spaceBetween: 0,
        freeMode: true,
        freeModeMomentum: false,
        autoplay: delay && {
            delay: delay,
        },
    });
    $this.data('swiper', api);
});
