(($)=>{
    Array.prototype.forEach.call(document.getElementsByClassName('navigation-catalog'), (block)=>{
        Array.from(block.getElementsByClassName('nav-panel'))
            .sort((a, b) => a.getElementsByTagName('LI').length - b.getElementsByTagName('LI').length)
            .forEach((elem) => block.append(elem));
    });
})($);
