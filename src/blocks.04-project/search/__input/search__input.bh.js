module.exports = function(bh) {
    bh.match('search__input', function(ctx, json) {
        ctx
            .tag('input')
            .attrs({
                'type': 'text',
                'name': ctx.tParam('ID'),
                'id': ctx.tParam('ID'),
                'data-target': '.search-panel',
                'placeholder': 'Более 100 000 товаров: введите название, артикул, действующее вещество',
            });
    });
};
