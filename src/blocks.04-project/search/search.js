import Open from './../../blocks.02-common/open/open';
(()=>{
    const SELECTOR = {
        DATA_TOGGLE: '.search__input',
    };
    let $target = $(SELECTOR.DATA_TOGGLE);
    $(document)
        .on('focus', SELECTOR.DATA_TOGGLE, Open.show)
        .on('keyup keydown', SELECTOR.DATA_TOGGLE, (event) => {
            $target.not(event.target).val(event.target.value);
        });
})();
