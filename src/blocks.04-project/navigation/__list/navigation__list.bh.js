module.exports = function(bh) {
    bh.match('navigation__list', function(ctx, json) {
        ctx.tag('ul').content(
            ctx.content().map((item)=>{
                return ctx.isSimple(item) ? {elem: 'item', content: item} : item;
            }),
            true
        );
    });
};
