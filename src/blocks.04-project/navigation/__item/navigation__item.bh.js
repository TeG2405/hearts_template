module.exports = function(bh) {
    bh.match('navigation__item', function(ctx, json) {
        ctx
            .tag('li')
            .tParam('ID', ctx.generateId(), true)
            .content({elem: 'link', content: ctx.content()}, ctx.isSimple(ctx.content()));
    });
};
