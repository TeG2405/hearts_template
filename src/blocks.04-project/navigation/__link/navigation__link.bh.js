module.exports = function(bh) {
    bh.match('navigation__link', function(ctx, json) {
        ctx
            .tag('a')
            .attr('href', ctx.attr('data-toggle') == 'open' ? '#'+ctx.tParam('ID') : '#');
    });
};
