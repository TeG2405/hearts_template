module.exports = function(bh) {
    bh.match('nav-panel__list', function(ctx, json) {
        ctx.tag('ul').content(
            ctx.content().map((item)=>{
                return {tag: 'li', content: item};
            }),
            Array.isArray(ctx.content())
        );
    });
};

