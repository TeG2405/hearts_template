module.exports = function(bh) {
    bh.match('nav-panel__collapse', function(ctx, json) {
        ctx
            .cls(`collapse d-${ctx.tParam('breakpoint')}-block`)
            .attrs({
                'id': ctx.tParam('ID'),
            });
    });
};
