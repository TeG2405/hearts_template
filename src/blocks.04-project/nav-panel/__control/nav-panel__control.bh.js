module.exports = function(bh) {
    bh.match('nav-panel__control', function(ctx, json) {
        ctx
            .tag('a')
            .cls(`d-${ctx.tParam('breakpoint')}-none collapsed`)
            .attrs({
                'href': '#'+ctx.tParam('ID'),
                'data-toggle': 'collapse',
            });
    });
};
