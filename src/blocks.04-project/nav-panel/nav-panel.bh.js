module.exports = function(bh) {
    bh.match('nav-panel', function(ctx, json) {
        ctx
            .tParam('ID', ctx.generateId(), true)
            .tParam('breakpoint', json.breakpoint || 'xs');
    });
};

