module.exports = function(bh) {
    bh.match('navigation-panel__li', function(ctx, json) {
        ctx
            .tag('li')
            .content({elem: 'a', content: ctx.content()}, ctx.isSimple(ctx.content()));
    });
};
