module.exports = function(bh) {
    bh.match('navigation-panel__control', function(ctx, json) {
        ctx
            .tag('a')
            .attrs({
                'data-toggle': 'open',
                'href': '#'+ctx.tParam('ID'),
            });
    });
};
