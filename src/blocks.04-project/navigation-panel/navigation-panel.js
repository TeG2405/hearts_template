import {Modal, Util} from 'bootstrap';
(($)=>{
    const modalApi = new Modal();
    const EVENT = {
        SHOW: 'show.iv.open',
        HIDE: 'hide.iv.open',
        FIXED: 'fixed.iv.fixedTopControl',
        STICK: 'sticky.iv.fixedTopControl',
    };
    const CLASS = {
        OPEN: 'navigation-panel-open',
        BACKDROP: 'navigation-panel-backdrop',
        FADE: 'fade',
        SHOW: 'show',
    };
    const SELECTOR = {
        SEARCH: '.search',
        NAVIGATION: '.navigation',
        BLOCK: '.navigation-panel',
        FIXED_CONTROL: '.fixed-top-control',
    };

    $(document)
        .on(EVENT.SHOW, SELECTOR.BLOCK, (event)=>{
            let $target = $(event.target);
            let isMobile = $target.css('position') == 'fixed';
            let isFixed = $target.closest(SELECTOR.NAVIGATION).css('position') == 'fixed';
            if ($target.is(SELECTOR.BLOCK)) {
                if (isMobile || isFixed) {
                    modalApi._checkScrollbar();
                    modalApi._setScrollbar();
                    $(document.body).addClass(CLASS.OPEN);
                }
                if (!modalApi._backdrop) {
                    modalApi._backdrop = document.createElement('div');
                    modalApi._backdrop.classList.add(CLASS.BACKDROP);
                    modalApi._backdrop.classList.add(CLASS.FADE);
                    if (isMobile) {
                        $target.after(modalApi._backdrop);
                    } else {
                        $(modalApi._backdrop).appendTo(document.body);
                    }
                    Util.reflow(modalApi._backdrop);
                    $(modalApi._backdrop).one('click', () => {
                        $target
                            .removeClass(CLASS.SHOW)
                            .trigger(new $.Event(EVENT.HIDE));
                    });
                }
                modalApi._backdrop.classList.add(CLASS.SHOW);
            }
        })
        .on(EVENT.HIDE, SELECTOR.BLOCK, (event)=>{
            let $target = $(event.target);
            if ($target.is(SELECTOR.BLOCK) && modalApi._backdrop) {
                $(document.body).removeClass(CLASS.OPEN);
                modalApi._resetScrollbar();
                modalApi._backdrop.classList.remove(CLASS.SHOW);
                $(modalApi._backdrop)
                    .one(Util.TRANSITION_END, () => {
                        modalApi._removeBackdrop();
                    })
                    .emulateTransitionEnd(Util.getTransitionDurationFromElement(modalApi._backdrop));
            }
        });
})($);
