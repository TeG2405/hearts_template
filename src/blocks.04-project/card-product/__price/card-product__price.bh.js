module.exports = function(bh) {
    bh.match('card-product__price', function(ctx, json) {
        let price = Math.floor(Math.random()*10000);
        ctx.content([
            {tag: 'del', content: price+' р.'},
            {block: 'text-primary', content: (price - Math.floor(price / 100 * Math.random()*100))+' р.'},
        ]);
    });
};
