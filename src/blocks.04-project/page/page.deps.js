module.exports = {
    shouldDeps: [
        {block: 'font-decima-rounda-a'},
        {block: 'font-open-sans'},
        {block: 'font-pt-sans'},
    ],
};
