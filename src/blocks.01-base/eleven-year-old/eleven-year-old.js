// Скрипт тестирования верстки на устойчевость к контенту
(() => {
    var a;
    var w = document.createTreeWalker(
        document.body,
        NodeFilter.SHOW_TEXT,
        {
            acceptNode: () => NodeFilter.FILTER_ACCEPT,
        },
        true
    );
    while (a = w.nextNode()) {
        if (a.textContent.trim().length) {
            a.textContent = 'Одиннадцатиклассница пошла посмотреть на достопримечательность, она шла долго, несколько строчек, пока не пришла';
        }
    }
})();
