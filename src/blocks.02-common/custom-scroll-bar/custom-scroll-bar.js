import SimpleBar from 'simplebar';
(()=>{
    const detectionCssScrollBar = () => {
        var test = document.createElement('div');
        var property = 'scrollbar{width:0px;}';
        var fake = false;
        var root = document.body || (()=>{
                fake = true;
                return document.documentElement.appendChild(document.createElement('body'));
            })();

        test.id = '__sb';
        test.style.overflow = 'scroll';
        test.style.width = '40px';
        test.innerHTML = `&shy;<style>#${test.id}::-webkit-${property}</style>`;
        root.appendChild(test);
        var ret = test.scrollWidth == 40;
        document.body.removeChild(test);
        if (fake) {
            document.documentElement.removeChild(root);
        }
        return ret;
    };
    const init = () => {
        Array.prototype.forEach.call(document.getElementsByClassName('custom-scroll-bar'), (item)=>{
            new SimpleBar(item);
        });
    };
    if (!detectionCssScrollBar()) {
        init();
        $(document).on('OnDocumentHtmlChanged', init);
    }
})();
