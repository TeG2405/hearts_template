import {Util} from 'bootstrap';
const Open = (($)=>{
    const EVENT = {
        SHOW: 'show.iv.open',
        HIDE: 'hide.iv.open',
    };
    const CLASS = {
        SHOW: 'show',
    };
    const SELECTOR = {
        DATA_TOGGLE: '[data-toggle="open"]',
    };
    class Open {
        static open(target) {
            $(Util.getSelectorFromElement(target)).addClass(CLASS.SHOW).trigger(new $.Event(EVENT.SHOW));
        }
        static close(target) {
            $(Util.getSelectorFromElement(target)).removeClass(CLASS.SHOW).trigger(new $.Event(EVENT.HIDE));
        }
        static show(event) {
            Open.open(event.currentTarget);
            event.preventDefault();
        }
        static hide(event) {
            Open.close(event.currentTarget);
            event.preventDefault();
        }
        static toggle(event) {
            if ($(Util.getSelectorFromElement(event.currentTarget)).hasClass(CLASS.SHOW)) {
                Open.close(event.currentTarget);
            } else {
                Open.open(event.currentTarget);
            }
            event.preventDefault();
        }
    }
    $(document)
        .on('click', SELECTOR.DATA_TOGGLE, Open.toggle);
    return Open;
})($);
export default Open;
